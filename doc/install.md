Prerequisites: `python3`; `gcc` and/or `clang`; OpenSSL. Currently
tested only under Linux, but porting to other systems shouldn't be
difficult.

### For sysadmins

To install in `/usr/local/{include,lib,bin}`:

    ./configure && make -j8 install

If you're running an old Linux system and see that the `randombytes-info`
output says `randombytes source kernel-devurandom` (this will happen on
Linux kernels before 3.17), add the lines

    dd count=1 bs=64 if=/dev/random of=/dev/urandom status=none \
    && findmnt -t tmpfs -T /var/run >/dev/null \
    && touch /var/run/urandom-ready &

to your boot scripts to improve librandombytes startup time. On new
Linux systems, `randombytes-info` should instead say `kernel-getrandom`
and startup time should be fine in any case, unaffected by these lines.

If you're running a Linux virtual machine (old or new) and see startup
delays, you probably need the host to provide `virtio-rng`.

### For developers with an unprivileged account

Typically you'll already have

    export LD_LIBRARY_PATH="$HOME/lib"
    export LIBRARY_PATH="$HOME/lib"
    export CPATH="$HOME/include"
    export PATH="$HOME/bin:$PATH"

in `$HOME/.profile`. To install in `$HOME/{include,lib,bin}`:

    ./configure --prefix=$HOME && make -j8 install

### For distributors creating a package

Run

    ./configure --prefix=/usr && make -j8

and then follow your usual packaging procedures for the
`build/0/package` files:

    build/0/package/man/man3/randombytes.3
    build/0/package/man/man1/randombytes-info.1
    build/0/package/include/randombytes.h
    build/0/package/lib/librandombytes*
    build/0/package/bin/randombytes-info

Note that `librandombytes-kernel` and `librandombytes-openssl` are
alternative implementations of the same librandombytes API. There are
default symlinks to `librandombytes-kernel`, but you should allow the
sysadmin to change these symlinks to `librandombytes-openssl` by simply
installing a `librandombytes-openssl` package. The OpenSSL dependency is
for `librandombytes-openssl`; the rest of librandombytes is independent
of OpenSSL.

### More options

You can run

    ./configure --host=amd64

to override `./configure`'s guess of the architecture that it should
compile for. However, cross-compilers aren't yet selected automatically.

Inside the `build` directory, `0` is symlinked to `amd64` for
`--host=amd64`. Running `make clean` removes `build/amd64`. Re-running
`./configure` automatically starts with `make clean`.

A subsequent `./configure --host=arm64` will create `build/arm64` and
symlink `0 -> arm64`, without touching an existing `build/amd64`.

Compilers tried are listed in `compilers/default`. Each compiler
includes `-fPIC` to create a shared library and `-fwrapv` to switch to a
slightly less dangerous version of C; also, `-fvisibility=hidden` is
added automatically to hide non-public symbols in the library. The first
compiler that seems to work is used to compile everything.
