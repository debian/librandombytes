### NAME

randombytes-info - report information about random-number generator

### SYNOPSIS

    randombytes-info

### DESCRIPTION

`randombytes-info`
prints human-readable information
about the RNG selected by `randombytes()`.

The format is subject to change
but currently includes
a `randombytes source` line,
`randombytes test_fork` lines
(which should show different random-looking data for `x` and `y`),
and `randombytes timing` lines.

### SEE ALSO

**randombytes**(3)
