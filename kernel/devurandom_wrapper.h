#ifndef devurandom_wrapper_h
#define devurandom_wrapper_h

#define devurandom_wrapper_ready randombytes_internal_devurandom_wrapper_ready
#define devurandom_wrapper randombytes_internal_devurandom_wrapper

#ifdef __cplusplus
extern "C" {
#endif

extern int devurandom_wrapper_ready(void);
extern void devurandom_wrapper(void *,long long);

#ifdef __cplusplus
}
#endif

#endif
