#ifndef getrandom_wrapper_h
#define getrandom_wrapper_h

#define getrandom_wrapper_ready randombytes_internal_getrandom_wrapper_ready
#define getrandom_wrapper randombytes_internal_getrandom_wrapper

#ifdef __cplusplus
extern "C" {
#endif

extern int getrandom_wrapper_ready(void);
extern void getrandom_wrapper(void *,long long);

#ifdef __cplusplus
}
#endif

#endif
